tinymce.PluginManager.load('moxiemanager', '/assets/vendor/tinymce/plugins/moxiemanager/plugin.min.js');

var tinymce_plugins = [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern moxiemanager"
];

var global_settings = {
	'plugins': tinymce_plugins,
	'relative_urls': false,
	'content_css': '/assets/styles/blocks_mce.css'
};

tinymces = {
	1: jQuery.extend(Object.create(global_settings), {
		'menubar': 'file edit view format',
		'toolbar': 'bold italic | styleselect | bullist numlist | image | code'
	}),
	2: jQuery.extend(Object.create(global_settings), {
		'menubar': 'file edit insert view format table tools',
		'toolbar1': 'undo redo | bold italic | styleselect',
		'toolbar2': 'bullist numlist | alignleft aligncenter alignright alignjustify | link forecolor | image'
	})
};