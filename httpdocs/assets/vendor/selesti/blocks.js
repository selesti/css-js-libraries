var page, blocks = {}, row = {}, column = {}, block = {}, widgets = {}, message = {}, loader = {};

blocks.init = function() {
	blocks.bindEvents();
	blocks.sortables();
	//blocks.datetimepickers();
	blocks.emptyNoteCheck();
	blocks.resizeIframe();
};
row.init = function() {
	row.bindEvents();
};
column.init = function() {
	column.bindEvents();
	column.expandColumnCheck();
};
block.init = function() {
	block.bindEvents();
	block.movedColumn = false;
	block.initInlineEditing();
};
widgets.init = function() {
	widgets.bindEvents();
};

blocks.bindEvents = function() {
	jQuery(window).load(function() {
		page.on('click', '.advanced-edit', blocks.advancedTools);
		page.on('click', '.publish', blocks.publish);
		page.on('click', '.history', blocks.history);
		page.on('click', '.restore', blocks.restore);
		page.on('click', '.blocks-modal-container .close', blocks.hideModal);
		page.on('click', '.device-previews', blocks.openDevicePreviewList);
		page.on('click', '.open-device-preview', blocks.openDevicePreview);
		page.on('click', '.button.has-checkbox', blocks.toggleButtonCheckbox);
		page.on('click', '.advanced-option', blocks.toggleAdvancedOptions);
		page.on('click', '.blocks-confirm-container .cancel', message.confirmCancel);

		window.location.search.indexOf('dev') != -1 && jQuery('.header-controls .advanced-edit').trigger('click');
	});
};
row.bindEvents = function() {
	jQuery(window).load(function() {
		page.on('click', '.add-row', row.add);
		page.on('click', '.add-row-cancel', row.addCancel);
		page.on('click', '.add-row-container .add', row.addLayout);
		page.on('click', '.add-page-break', row.addPageBreak);
		page.on('click', '.row-controls .status', row.status);
		page.on('click', '.row-controls .delete', row.delete);
		page.on('click', '.row-controls .delete-perm', row.deletePerm);
		page.on('click', '.row-controls .toggle-advanced', row.toggleAdvanced);
		page.on('change', '.global_override', row.globalOverride);
		page.on('click', '.advanced-row-option-controls .save-device-visibility', row.saveDeviceVisibility);
		page.on('click', '.advanced-row-option-controls .save-activation-dates', row.saveActivationDates);
	});
};
column.bindEvents = function() {
	jQuery(window).load(function() {
		page.on('click', '.expand-column', column.expandColumn);
	});
};
block.bindEvents = function() {
	jQuery(window).load(function() {
		page.on('click', '.add-block', block.add);
		page.on('blur', '.visible-content[contenteditable]', block.editInline);
		page.on('click', '.block-controls .edit', block.edit);
		page.on('click', '.block-controls .status', block.status);
		page.on('click', '.block-controls .delete', block.delete);
		page.on('click', '.block-controls .delete-perm', block.deletePerm);
		page.on('click', '.block-controls .responsive', block.responsive);
		page.on('click', '.edit-controls .save', block.save);
		page.on('click', '.edit-controls .cancel', block.cancel);
		page.on('click', '.control-advanced .toggle-advanced', block.toggleAdvanced);
		page.on('click', '.advanced-block-option-controls .save-device-visibility', block.saveDeviceVisibility);
		page.on('click', '.advanced-block-option-controls .save-activation-dates', block.saveActivationDates);
	});
};
widgets.bindEvents = function() {
	jQuery(window).load(function() {
		page.on('click', '.submit-widget-search', widgets.list);
		page.on('keydown', '#widget_search', widgets.searchKeydown);
		page.on('click', '.clear-widget-search', widgets.clearSearch);
		page.on('click', '.cancel-widget, .widget-list-overlay', widgets.close);
		page.on('click', '.insert-widget', widgets.choose);
	});
};

blocks.advancedTools = function() {
	jQuery('.block-block.editing').removeClass('editing');
	jQuery('.controls, .add-block-container').toggle(!jQuery('.header-controls .advanced-edit').hasClass('active'));
	jQuery('.block-content .visible-content').show();
	jQuery('.block-content .editable-content').hide();
	jQuery('.advanced-row-options-container:visible').parents('.block-row').find('.toggle-advanced').trigger('click');
	jQuery('.advanced-block-options-container:visible').parents('.block-block').find('.toggle-advanced').trigger('click');
	jQuery('.add-row-container').hide();
	tinymce.remove('.editable-content textarea');

	jQuery('.header-controls .advanced-edit').toggleClass('active');
	jQuery('.block-rows-container').toggleClass('advanced');

};
blocks.publish = function() {
	that = this;
	if(jQuery('.block-block.editing').length)
	{
		message.confirm('Please save all blocks before publishing.', 'Okay', '', message.confirmCancel);
	}
	else
	{
		data = jQuery(that).data();
		confirm_message = 'Are you sure you want to publish the blocks?';
		url = '/blocks/publish/'+data.type+'/'+data.id;
		if(jQuery(that).hasClass('publish-translation'))
		{
			confirm_message += ' Only blocks for \''+jQuery(that).data('text')+'\' will be published.';
			url += '/'+data.language+'/'+data.site;
		}
		message.confirm(confirm_message, 'Yes', 'No', function() {
			loader.show();
			jQuery.post('/blocks/publish/'+data.type+'/'+data.id, function(data) {
				jQuery('.block-row.deleted, .block-block.deleted').remove();
				if(data.extra.show_history)
				{
					jQuery('.header-controls .history').removeClass('hidden');
				}
				message.show(data.message);
				loader.hide();
			});
		});
	}
};
blocks.history = function() {
	loader.show();
	data = jQuery(this).data();
	jQuery.post('/blocks/history/'+data.type+'/'+data.id, function(data) {
		jQuery(data.extra.versions).each(function(i, version) {
			li = jQuery('<li/>');
			jQuery('<a/>', {'href': window.location.pathname+window.location.search+'&version='+version.id, 'text':version.date}).appendTo(li);
			jQuery('.version-history-versions').append(li);
		});
		jQuery('.version-history').show();
		loader.hide();
	});
};
blocks.restore = function() {
	that = this;
	message.confirm('Are you sure you want to restore this version? <u>Any un-published changes made in preview mode will be lost.</u><br /><br />Only the preview version of this page will be restored; the live page will not be changed.', 'Yes', 'No', function() {
		loader.show();
		message.show('The page is being restored. You will be redirected shortly...');
		data = jQuery(that).data();
		jQuery.post('/blocks/restore/'+data.type+'/'+data.id+'/'+data.archiveVersionId, function(data) {
			window.location = window.location.pathname+'?cms_admin';
		});
	});
};
blocks.emptyNoteCheck = function() {
	jQuery('.empty-note').removeClass('show-note');
	jQuery('.block-rows-container').each(function(i, row_set) {
		if(jQuery(row_set).find('.block-row:not(.inactive):not(.deleted) .block-block:not(.inactive):not(.deleted)').length == 0)
		{
			jQuery(row_set).find('.empty-note').addClass('show-note');
		}
	});
};
blocks.hideModal = function() {
	jQuery('.blocks-modal-container:visible').hide();
};
blocks.openDevicePreviewList = function() {
	jQuery('.device-visibility-devices').show();
};
blocks.openDevicePreview = function() {
	url = '/'+jQuery(this).data('url');
	window_width = jQuery(this).data('width');
	window_height = jQuery(this).data('height');
	window_left = (window.outerWidth / 2) - (window_width / 2);
	window_top = (window.outerHeight / 2) - (window_height / 2);
	window.open(url, '', 'width='+window_width+', height='+window_height+', left='+window_left+', top='+window_top);
};
blocks.toggleButtonCheckbox = function(e) {
	jQuery(e.target).is('.button') && jQuery(this).find('input[type="checkbox"]').prop('checked', !jQuery(this).find('input[type="checkbox"]').prop('checked'));
};
blocks.toggleAdvancedOptions = function() {
	already_open = jQuery(this).parents('.advanced-options-container').find('.option-'+jQuery(this).data('toggle')).is(':visible');
	jQuery(this).parents('.advanced-options-container').find('.advanced-option-container').hide();
	!already_open && jQuery(this).parents('.advanced-options-container').find('.option-'+jQuery(this).data('toggle')).show();
};
blocks.sortables = function() {
	jQuery('.block-rows-container').sortable({
		'axis': 'y',
		'handle': '.move-row',
		'items': '.block-row',
		'placeholder': 'placeholder',
		'tolerance': 'pointer',
		'start': function(event, ui) {
			jQuery('.block-controls').hide();
			jQuery('.placeholder').css({'height': jQuery(ui.item).height()});
		},
		'stop': function(event, ui) {
			data = {};
			data.sort = jQuery(this).sortable('toArray', {'attribute': 'data-id'});
			data.set_id = jQuery(this).data('id');
			loader.show();
			jQuery.post('/row/sort', data, function(data) {
				jQuery('.controls, .add-block-container').show();
				message.show(data.message);
				loader.hide();
			});
		}
	});
	jQuery('.block-row').sortable({
		'axis': 'x',
		'containment': 'parent',
		'handle': '.move-column',
		'items': '.block-column',
		'placeholder': 'placeholder',
		'tolerance': 'pointer',
		'start': function(event, ui) {
			jQuery('.block-controls').hide();
			jQuery('.placeholder')
			.attr('class',
				jQuery('.placeholder').attr('class')+' '+jQuery(ui.item).attr('class')
			)
			.css({
				'height':
					jQuery(ui.item).find('.block-column-blocks').outerHeight()+
			 		(jQuery(ui.item).outerWidth()-jQuery('.placeholder').width()-4),
			 	'marginTop': 25
			});
		},
		'stop': function(event, ui) {
			data = {};
			data.sort = jQuery(this).sortable('toArray', {'attribute': 'data-id'});
			loader.show();
			jQuery.post('/column/sort', data, function(data) {
				jQuery('.controls, .add-block-container').show();
				column.expandColumnCheck();
				message.show(data.message);
				loader.hide();
			});
		},
		'zIndex': 200
	});
	jQuery('.block-column-blocks').sortable({
		'handle': '.move-block',
		'items': '.block-block',
		'connectWith': '.block-column-blocks',
		'placeholder': 'placeholder',
		'tolerance': 'pointer',
		'start': function(event, ui) {
			switch(ui.item.data('type'))
			{
				case 'embed':
					jQuery(ui.item).find('.visible-content').html('');
					break;
				case 'widget':
					if(jQuery(ui.item).data('widgetAvailableColumns'))
					{
						available_columns = jQuery(ui.item).data('widgetAvailableColumns').toString().split(',');
						jQuery('.block-column-blocks').addClass('no-sort-drop');
						jQuery(available_columns).each(function(i, width) {
							jQuery('.block-column[data-width="'+width+'"] .block-column-blocks').removeClass('no-sort-drop');
						});
					}
					break;
			}
			block.movedColumn = false;
			jQuery('.block-column-blocks').addClass('sorting');
			jQuery('.block-controls').hide();
			jQuery('.placeholder').css({'height': jQuery(ui.item).height()});
		},
		'stop': function(event, ui) {
			switch(jQuery(ui.item).data('type'))
			{
				case 'widget':
					if(jQuery(ui.item).parents('.block-column').find('.block-column-blocks').hasClass('no-sort-drop'))
					{
						jQuery('.block-column-blocks').removeClass('sorting no-sort-drop');
						jQuery('.controls, .add-block-container').show();

						return false;
					}

					break;
			}
			data = {};
			data.original_column = {};
			data.original_column.id = jQuery(this).parents('.block-column').data('id');
			data.original_column.sort = jQuery(this).sortable('toArray', {'attribute': 'data-id'});
			if(block.movedColumn)
			{
				data.new_column = {};
				data.new_column.id = jQuery(ui.item).parents('.block-column').data('id');
				data.new_column.sort = jQuery(ui.item).parents('.block-column').find('.block-column-blocks').sortable('toArray', {'attribute': 'data-id'});
			}
			switch(ui.item.data('type'))
			{
				case 'embed':
					jQuery(ui.item).find('.visible-content').html(jQuery(ui.item).find('.editable-content textarea').val());
					break;
			}
			loader.show();
			jQuery.post('/block/sort', data, function(data) {
				jQuery('.block-column-blocks').removeClass('sorting no-sort-drop');
				jQuery('.controls, .add-block-container').show();
				column.expandColumnCheck();
				message.show(data.message);
				loader.hide();
			});
		},
		'receive': function(event, ui) {
			block.movedColumn = true;
		},
		'over': function(event, ui) {
			if(jQuery(event.target).hasClass('no-sort-drop'))
			{
				jQuery(ui.item).addClass('sortable-no-drop');
			}
		},
		'out': function(event, ui) {
			jQuery(ui.item).removeClass('sortable-no-drop');
		},
		'zIndex': 200
	});
};
blocks.datetimepickers = function() {
	jQuery('.activation-dates-datepicker-container input[type="text"]').datepicker({
		'dateFormat': 'dd/mm/yy',
		//'controlType': 'select',
		'onSelect': function(date, event) {
			element = jQuery(event.input.context);
			switch(element.data('type'))
			{
				case 'activation':
					element.parents('.activation-dates-datepicker-container').find('[data-type="deactivation"]').datepicker('option', 'minDate', element.datepicker('getDate'));
					break;
				case 'deactivation':
					element.parents('.activation-dates-datepicker-container').find('[data-type="activation"]').datepicker('option', 'maxDate', element.datepicker('getDate'));
					break;
			}
		}
	});
	jQuery('.activation-dates-datepicker-container').each(function(i, dates) {
		activation = jQuery(dates).find('[data-type="activation"]');
		deactivation = jQuery(dates).find('[data-type="deactivation"]');
		if(activation.val())
		{
			deactivation.datepicker('option', 'minDate', activation.datepicker('getDate'));
		}
		else
		{
			deactivation.datepicker('option', 'minDate', 'today');
		}
		if(deactivation.val())
		{
			activation.datepicker('option', 'maxDate', deactivation.datepicker('getDate'));
		}
		activation.datepicker('option', 'minDate', 'today');
	});
};

row.add = function() {
	new_row_columns = jQuery(this).parents('.block-rows-container').find('.add-row-container').clone();
	new_row_columns.addClass('new-add-row-container');
	jQuery('.add-row').hide();
	if(jQuery(this).parents('.block-rows').length)
	{
		jQuery(this).parents('.block-row').before(new_row_columns);
	}
	else
	{
		jQuery(this).parents('.block-rows-container').find('.block-rows').append(new_row_columns);
	}
	jQuery('.new-add-row-container').show().removeClass('new-add-row-container');
	blocks.resizeIframe();
};
row.addCancel = function() {
	jQuery(this).parents('.add-row-container').remove();
	jQuery('.add-row').show();
	blocks.resizeIframe();
};
row.addLayout = function() {
	that = this;
	block_rows_container_data = jQuery(this).parents('.block-rows-container').data();
	loader.show();
	jQuery.post('/row/add', {
		'layout': jQuery(this).data('id'),
		'row_set_id': block_rows_container_data.id,
		'row_set_item_type': block_rows_container_data.itemType,
		'row_set_item_id': block_rows_container_data.itemId,
		'row_set_item_url': block_rows_container_data.itemUrl,
		'row_set_item_page_id': block_rows_container_data.itemPageId,
		'row_set_name': block_rows_container_data.name,
		'pagebreak': 0,
		'pagebreaks': block_rows_container_data.pagebreaks
	}, function(data) {
		success_message = data.message;
		add_row_container = jQuery(that).parents('.add-row-container');
		add_row_container.after(data.html);
		new_row = add_row_container.next('.block-row');
		add_row_container.remove();
		if(!block_rows_container_data.id)
		{
			new_row.parents('.block-rows-container').attr('data-id', data.extra.row_set_id);
			new_row.parents('.block-rows-container').data('id', data.extra.row_set_id);
		}
		blocks.resizeIframe();

		blocks.sortables();
		new_row.parents('.block-rows-container').sortable('refresh');
		data = {};
		data.sort = new_row.parents('.block-rows-container').sortable('toArray', {'attribute': 'data-id'});
		data.set_id = new_row.parents('.block-rows-container').data('id');
		jQuery.post('/row/sort', data, function(data) {
			jQuery('.add-row').show();
			column.expandColumnCheck();
			message.show(success_message);
			loader.hide();
		});
	});
};
row.addPageBreak = function() {
	that = this;
	block_rows_container_data = jQuery(this).parents('.block-rows-container').data();
	loader.show();
	jQuery.post('/row/add', {
		'row_set_id': block_rows_container_data.id,
		'pagebreak': 1,
		'pagebreaks': block_rows_container_data.pagebreaks
	}, function(data) {
		success_message = data.message;
		new_row = jQuery(that).parents('.block-row').before(data.html);
		blocks.resizeIframe();

		blocks.sortables();
		new_row.parents('.block-rows-container').sortable('refresh');
		data = {};
		data.sort = new_row.parents('.block-rows-container').sortable('toArray', {'attribute': 'data-id'});
		data.set_id = new_row.parents('.block-rows-container').data('id');
		jQuery.post('/row/sort', data, function(data) {
			jQuery('.add-row').show();
			message.show(success_message);
			loader.hide();
		});
	});
}
row.status = function() {
	that = this;
	loader.show();
	jQuery.post('/row/status/'+jQuery(that).parents('.block-row').data('id'), function(data) {
		jQuery(that).parents('.block-row').toggleClass('inactive');
		blocks.toggleText(that);
		blocks.emptyNoteCheck();
		message.show(data.message);
		loader.hide();
	});
};
row.delete = function() {
	that = this;
	loader.show();
	jQuery.post('/row/delete/'+jQuery(that).parents('.block-row').data('id'), function(data) {
		jQuery(that).parents('.block-row').toggleClass('deleted');
		blocks.toggleText(that);
		jQuery(that).parent().find('.status').toggleClass('hidden');
		jQuery(that).parent().find('.delete-perm').toggleClass('hidden');
		blocks.emptyNoteCheck();
		message.show(data.message);
		loader.hide();
	});
};
row.deletePerm = function() {
	that = this;
	message.confirm('Are you sure you want to permanently delete this row? It will not be removed from the live page until you publish.', 'Yes', 'No', function() {
		loader.show();
		jQuery.post('/row/deletePerm/'+jQuery(that).parents('.block-row').data('id'), function(data) {
			success_message = data.message;
			rows_container = jQuery(that).parents('.block-rows-container');
			jQuery(that).parents('.block-row').remove();

			data = {};
			data.sort = rows_container.sortable('toArray', {'attribute': 'data-id'});
			data.set_id = rows_container.data('id');
			jQuery.post('/row/sort', data, function(data) {
				blocks.emptyNoteCheck();
				message.show(success_message);
				loader.hide();
			});
		});
	});
};
row.toggleAdvanced = function() {
	jQuery(this).parents('.block-row').toggleClass('viewing-advanced-row-options').find('.advanced-row-options-container').toggle();
	jQuery(this).parents('.block-row').find('.advanced-row-option').hide();
	blocks.toggleText(this);
};
row.saveDeviceVisibility = function() {
	that = this;
	devices = [];
	jQuery(this).parents('.block-row').find('.row-device-visibility li input[type="checkbox"]:checked').each(function(i, device) {
		devices.push(jQuery(device).val());
	});
	loader.show();
	jQuery.post('/blocks/device_visibility/row/'+jQuery(that).parents('.block-row').data('id'), {'devices': devices.join(',')}, function(data) {
		jQuery(that).parents('.block-row').find('.row-device-visibility').hide();
		jQuery(that).parents('.block-row').toggleClass('has-device-visibility', !!devices.length);
		message.show(data.message);
		loader.hide();
	});
};
row.saveActivationDates = function() {
	that = this;
	loader.show();
	activation = jQuery(that).parents('.block-row').find('.row-activation-dates').find('[data-type="activation"]').val();
	deactivation = jQuery(that).parents('.block-row').find('.row-activation-dates').find('[data-type="deactivation"]').val();
	jQuery.post('/blocks/activation_dates/row/'+jQuery(that).parents('.block-row').data('id'), {'activation': activation, 'deactivation': deactivation}, function(data) {
		jQuery(that).parents('.block-row').find('.row-activation-dates').hide();
		jQuery(that).parents('.block-row').toggleClass('has-activation-dates', (activation||deactivation));
		message.show(data.message);
		loader.hide();
	});
};
row.globalOverride = function() {
	that = this;
	message.confirm('Are you sure you want to '+((jQuery(that).is(':checked'))?'override':'revert to')+' the global blocks for this page?', 'Yes', 'No',
	function() {
		loader.show();
		block_rows_container_data = jQuery(that).parents('.block-rows-container').data();
		jQuery.post('/row/globalOverride', {
			'row_set_id': block_rows_container_data.id,
			'row_set_item_type': block_rows_container_data.itemType,
			'row_set_item_id': block_rows_container_data.itemId,
			'row_set_item_url': block_rows_container_data.itemUrl,
			'row_set_item_page_id': block_rows_container_data.itemPageId
		}, function(data) {
			window.location.reload();
		});
	},
	function() {
		jQuery(that).attr('checked', !jQuery(that).is(':checked'));
	});
};

column.expandColumnCheck = function() {
	jQuery('.expand-column').hide();
	jQuery('.block-row-columns').each(function(i, columns) {
		jQuery(columns).find('.block-column').each(function(k, column) {
			show_expand = false;
			if(jQuery(column).find('.block-column-blocks .block-block').length == 0 && jQuery(column).prev('.block-column').length)
			{
				if(jQuery(column).prev('.block-column').find('.block-column-blocks .block-block.block-type-widget').length)
				{
					can_fit = true;
					new_width = parseInt(jQuery(column).data('width')) + parseInt(jQuery(column).prev('.block-column').data('width'));
					all_available_columns = [];
					jQuery(column).prev('.block-column').find('.block-column-blocks .block-block.block-type-widget').each(function(i, widget_block) {
						widget_available_columns = jQuery(widget_block).data('widgetAvailableColumns').toString().split(',');
						if(widget_available_columns.indexOf(new_width.toString()) == -1)
						{
							can_fit = false;
						}
					});
					if(can_fit)
					{
						show_expand = true;
					}
				}
				else
				{
					show_expand = true;
				}
			}
			if(show_expand)
			{
				jQuery(column).prev('.block-column').find('.expand-column.'+jQuery(column).prev('.block-column').attr('class').match(/column-(narrow|wide)/)[1]+'-button').show();
			}
		});
	});
};
column.expandColumn = function() {
	that = this;
	keep_id = jQuery(this).parents('.block-column').data('id');
	remove_id = jQuery(this).parents('.block-column').next('.block-column').data('id');
	loader.show();
	jQuery.post('/column/expand/'+keep_id+'/'+remove_id, function(data) {
		jQuery(that).parents('.block-column').next('.block-column').remove();
		jQuery(that).parents('.block-column').attr('class', data.extra.class);
		jQuery(that).parents('.block-column').attr('data-width', data.extra.width);
		jQuery(that).parents('.block-column').data('width', data.extra.width);
		jQuery(that).parents('.block-column').attr('data-tinymce', data.extra.tinymce);
		jQuery(that).parents('.block-column').data('tinymce', data.extra.tinymce);
		jQuery(that).parents('.block-column').find('.add-block-container').remove();
		if(data.html)
		{
			jQuery(that).parents('.block-column').find('.block-column-blocks').after(data.html);
		}
		column.expandColumnCheck();
		message.show(data.message);
		loader.hide();
	});
};

block.add = function() {
	that = this;
	loader.show();
	jQuery.post('/block/add/'+jQuery(this).parents('.block-column').data('id')+'/'+jQuery(this).data('type'), function(data) {
		jQuery(that).parents('.block-column').find('.block-column-blocks').append(data.html).sortable('refresh');
		row_set = jQuery(that).parents('.block-rows-container');
		if(row_set.hasClass('single') && !row_set.hasClass('multiple'))
		{
			jQuery(that).parents('.add-block-container').hide();
		}
		jQuery(that).parents('.block-column').find('.block-column-blocks .block-block').last().find('.edit:visible').trigger('click');
		blocks.emptyNoteCheck();
		column.expandColumnCheck();
		blocks.resizeIframe();
		loader.hide();
	});
};
block.editInline = function() {
	that = this;
	loader.show();
	jQuery.post('/block/content/'+jQuery(this).parents('.block-block').data('id'), {'content': jQuery(this).html(), 'language': jQuery(this).parents('.block-rows-container').data('language-id'), 'site': jQuery(this).parents('.block-rows-container').data('site-id')}, function(data) {
		jQuery(that).parents('.block-content').find('.editable-content textarea').val(jQuery(that).html());
		message.show(data.message);
		loader.hide();
	});
};
block.edit = function() {
	element = jQuery(this).parents('.block-block');
	switch(element.data('type'))
	{
		case 'text':
			block.toggleEditMode(element);
			height = element.find('.visible-content').height();
			if(element.data('type') == 'text')
			{
				block.initTinymce(element, height);
			}
			break;
		case 'embed':
			block.toggleEditMode(element);
			element.find('.editable-content textarea').height(element.find('.editable-content textarea').width() / 2);
			break;
		case 'widget':
			element.addClass('widget-block');
			widgets.list();
			break;
	}
	blocks.resizeIframe();
};
block.status = function() {
	that = this;
	loader.show();
	jQuery.post('/block/status/'+jQuery(that).parents('.block-block').data('id'), function(data) {
		jQuery(that).parents('.block-block').toggleClass('inactive');
		blocks.toggleText(that);
		blocks.emptyNoteCheck();
		message.show(data.message);
		loader.hide();
	});
};
block.delete = function() {
	that = this;
	loader.show();
	jQuery.post('/block/delete/'+jQuery(that).parents('.block-block').data('id'), function(data) {
		jQuery(that).parents('.block-block').toggleClass('deleted');
		blocks.toggleText(that);
		jQuery(that).parents('.block-block').find('.control-move, .control-edit, .status, .control-device-visibility').toggleClass('hidden');
		jQuery(that).parents('.block-block').find('.delete-perm').toggleClass('hidden');
		blocks.emptyNoteCheck();
		message.show(data.message);
		loader.hide();
	});
};
block.deletePerm = function() {
	that = this;
	message.confirm('Are you sure you want to permanently delete this block? It will not be removed from the live page until you publish.', 'Yes', 'No', function() {
		block.doDeletePerm(that);
	});
};
block.doDeletePerm = function(that) {
	loader.show();
	jQuery.post('/block/deletePerm/'+jQuery(that).parents('.block-block').data('id'), function(data) {
		success_message = data.message;
		block_column = jQuery(that).parents('.block-column');
		jQuery(that).parents('.block-block').remove();

		data = {};
		data.original_column = {};
		data.original_column.id = block_column.data('id');
		data.original_column.sort = block_column.find('.block-column-blocks').sortable('toArray', {'attribute': 'data-id'});
		jQuery.post('/block/sort', data, function(data) {
			column.expandColumnCheck();
			blocks.emptyNoteCheck();
			message.show(success_message);
			loader.hide();
		});
	});
};
block.responsive = function() {
	that = this;
	loader.show();
	jQuery.post('/block/responsive/'+jQuery(that).parents('.block-block').data('id'), function(data) {
		jQuery(that).parents('.block-block').find('.block-content .visible-content').toggleClass('embed-responsive embed-responsive-16by9', jQuery(that).parents('.block-block').find('.responsive input[type="checkbox"]').is(':checked'));
		message.show(data.message);
		loader.hide();
	});
};
block.toggleAdvanced = function() {
	jQuery(this).parents('.block-block').toggleClass('viewing-advanced-block-options').find('.advanced-block-options-container').toggle();
	jQuery(this).parents('.block-block').find('.advanced-block-option').hide();
	blocks.toggleText(this);
};
block.saveDeviceVisibility = function() {
	that = this;
	devices = [];
	jQuery(this).parents('.block-block').find('.block-device-visibility li input[type="checkbox"]:checked').each(function(i, device) {
		devices.push(jQuery(device).val());
	});
	loader.show();
	jQuery.post('/blocks/device_visibility/block/'+jQuery(that).parents('.block-block').data('id'), {'devices': devices.join(',')}, function(data) {
		jQuery(that).parents('.block-block').find('.block-device-visibility').hide();
		jQuery(that).parents('.block-block').toggleClass('has-device-visibility', !!devices.length);
		message.show(data.message);
		loader.hide();
	});
};
block.saveActivationDates = function() {
	that = this;
	loader.show();
	activation = jQuery(that).parents('.block-block').find('.block-activation-dates').find('[data-type="activation"]').val();
	deactivation = jQuery(that).parents('.block-block').find('.block-activation-dates').find('[data-type="deactivation"]').val();
	jQuery.post('/blocks/activation_dates/block/'+jQuery(that).parents('.block-block').data('id'), {'activation': activation, 'deactivation': deactivation}, function(data) {
		jQuery(that).parents('.block-block').find('.block-activation-dates').hide();
		jQuery(that).parents('.block-block').toggleClass('has-activation-dates', (activation||deactivation));
		message.show(data.message);
		loader.hide();
	});
};
block.save = function() {
	that = this;
	element = jQuery(this).parents('.block-block');
	tinymce.triggerSave();
	if(!jQuery(element).find('.editable-content textarea').val())
	{
		message.confirm('This block is empty, do you want to delete it?', 'Yes', 'No',
		function() {
			block.doDeletePerm(that);
		},
		function() {
			block.doSave(element);
		});
	}
	else
	{
		block.doSave(element);
	}
};
block.doSave = function(element) {
	loader.show();
	jQuery.post('/block/content/'+element.data('id'), {'content': jQuery(element).find('.editable-content textarea').val(), 'language': jQuery(element).parents('.block-rows-container').data('language-id'), 'site': jQuery(element).parents('.block-rows-container').data('site-id')}, function(data) {
		jQuery(element).find('.visible-content').html(jQuery(element).find('.editable-content textarea').val());
		block.toggleEditMode(element);
		blocks.resizeIframe();
		message.show(data.message);
		loader.hide();

		switch(element.data('type'))
		{
			case 'text':
				tinymce.remove('#'+element.attr('id')+' .editable-content textarea');
				break;
			case 'embed':
				element.find('.editable-content textarea').height(element.find('.editable-content textarea').width() / 2);
				break;
		}
	});
};
block.cancel = function() {
	that = this;
	message.confirm('Are you sure you want to cancel?', 'Yes', 'No', function() {
		element = jQuery(that).parents('.block-block');
		switch(element.data('type'))
		{
			case 'text':
				tinymce.remove('#'+element.attr('id')+' .editable-content textarea');
				jQuery(element).find('.editable-content textarea').val(jQuery(element).find('.visible-content').html());
				break;
			case 'embed':
				jQuery(element).find('.editable-content textarea').val(jQuery(element).find('.visible-content').html());
				break;
		}
		block.toggleEditMode(element);
		blocks.resizeIframe();
	});
};
block.toggleEditMode = function(block) {
	block.toggleClass('editing');
	block.find('.block-controls').toggle();
	block.find('.block-content > div').toggle();
};
block.initInlineEditing = function() {
	if(jQuery('.block-header').hasClass('viewing-archive'))
	{
		return;
	}
	jQuery('.block-block.block-type-text').each(function(i, block) {
		init_config = {
			'selector': '#'+jQuery(block).attr('id')+' .visible-content',
			inline: true
		};
		jQuery.extend(init_config, tinymces[jQuery(block).parents('.block-column').data('tinymce')]);
		tinymce.init(init_config);
	});
};
block.initTinymce = function(block, height) {
	tinymce.remove('#'+block.attr('id')+' .editable-content textarea');
	init_config = {
		'selector': '#'+block.attr('id')+' .editable-content textarea',
		'height': height
	};
	jQuery.extend(init_config, tinymces[jQuery(block).parents('.block-column').data('tinymce')]);
	tinymce.init(init_config);
};

widgets.list = function() {
	search = jQuery('#widget_search').length?jQuery('#widget_search').val():'';
	width = jQuery('.widget-block').parents('.block-column').data('width');
	loader.show();
	jQuery.post('/widgets/search', {'width': width, 'search': search}, function(data) {
		jQuery('.blocks-widget-list-container').html(data).show();
		jQuery('#widget_search').focus();
		loader.hide();
	});
};
widgets.close = function() {
	jQuery('.blocks-widget-list-container').hide().html('');
	jQuery('.widget-block').removeClass('widget-block');
};
widgets.choose = function() {
	element = jQuery('.widget-block');
	that = this;
	loader.show();
	jQuery.post('/widgets/insert/'+element.data('id')+'/'+jQuery(this).data('type')+'/'+jQuery(this).data('id'), function(data) {
		jQuery(element).find('.visible-content').html(data.html);
		jQuery(element).attr('data-widget-available-columns', data.extra.widget_available_columns);
		jQuery(element).data('widgetAvailableColumns', data.extra.widget_available_columns);
		widgets.close();
		widgets.run(element, jQuery(this).data('type'), jQuery(this).data('id'));
		column.expandColumnCheck();
		blocks.resizeIframe();
		message.show(data.message);
		loader.hide();
	});
};
widgets.searchKeydown = function(e) {
	if(e.keyCode == 13)
	{
		widgets.list();
		return false;
	}
};
widgets.clearSearch = function() {
	jQuery('#widget_search').val('');
	widgets.list();
};

message.show = function(text) {
	jQuery('.blocks-messages-container').show();
	var id = 'message'+new Date().getTime();
	m = jQuery('<p/>', {'text':text,'id':id});
	m.appendTo('.blocks-messages-container .messages');
	m.slideDown(500);
	setTimeout(function() {
		message.hide(id);
	}, 3000);
};
message.hide = function(id) {
	jQuery('#'+id).slideUp(500, function() {
		jQuery('#'+id).remove();
	});
};
message.confirm = function(confirm_message, confirm_button_text, cancel_button_text, confirm_function, cancel_function) {
	cancel_function = cancel_function || jQuery.noop;
	jQuery('.blocks-messages-container').hide();
	jQuery('.blocks-confirm-overlay').show();
	jQuery('.blocks-confirm-container p').html(confirm_message);
	jQuery('.blocks-confirm-container .confirm').toggle(confirm_button_text != '');
	jQuery('.blocks-confirm-container .cancel').toggle(cancel_button_text != '');
	jQuery('.blocks-confirm-container .confirm').text(confirm_button_text);
	jQuery('.blocks-confirm-container .cancel').text(cancel_button_text);
	jQuery('.blocks-confirm-container .confirm').bind('click', message.confirmCancel);
	jQuery('.blocks-confirm-container .confirm').bind('click', confirm_function);
	jQuery('.blocks-confirm-container .cancel').bind('click', cancel_function);
	jQuery('.blocks-confirm-container').show();
};
message.confirmCancel = function() {
	jQuery('.blocks-confirm-container').hide();
	jQuery('.blocks-confirm-container .confirm').unbind('click');
	jQuery('.blocks-confirm-container .cancel').unbind('click');
	jQuery('.blocks-confirm-container .cancel').bind('click', message.confirmCancel);
	jQuery('.blocks-confirm-container').find('p, .confirm, .cancel').text('');
	jQuery('.blocks-confirm-overlay').hide();
};

loader.show = function() {
	jQuery('.blocks-ajax-overlay').fadeIn(500);
};
loader.hide = function() {
	jQuery('.blocks-ajax-overlay').fadeOut(500);
};

blocks.resizeIframe = function() {
	if(window.location.search.match(/iframe/))
	{
		window.parent.document.getElementById('blocks-'+window.location.search.match(/set=([a-z\_]+)/)[1]).height = jQuery('.block-rows-container').outerHeight() + 50;
	}
};
blocks.toggleText = function(button) {
	button = jQuery(button);
	currenttext = jQuery(button).text();
	toggletext = jQuery(button).data('toggletext');
	jQuery(button).text(toggletext);
	jQuery(button).data('toggletext', currenttext);
};

jQuery(function() {
	page = jQuery('body');

	blocks.init();
	row.init();
	column.init();
	block.init();
	widgets.init();

	jQuery('.header-controls .back a').mouseenter(function() {
		jQuery('.header-controls .back a').animate({'left':0}, 100);
	});
	jQuery('.header-controls .back a').mouseleave(function() {
		jQuery('.header-controls .back a').stop().animate({'left':-32}, 100);
	});
});