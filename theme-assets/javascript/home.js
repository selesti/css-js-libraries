home = {

    init: function(){
        this.bindEvents();
    },

    bindEvents: function(){

        // Custom $(document).ready callback
        selesti.ready(function(){

        });


        // Custom $(window).loaded callback
        selesti.loaded(function(){


        });


        // Custom $(window).resize callback
        selesti.resize(function() {


        });
    }

 };

home.init();