var sliders = {

	//define an empty list item to store them all in
	sliders: [],

	//create the initise function to sort everything out
	init: function(){

		selesti.ready(function(){
			sliders.bindEvents();
			sliders.genericSliders();
		});

		selesti.loaded(function() {
			sliders.resizeThumbnailPaginationMobile();
		});


		selesti.resize(function() {
			sliders.resizeThumbnailPaginationMobile();
		});
	},

	bindEvents: function(){

	},

	genericSliders: function(){

		var s = $('.owl-carousel:not(.owl-unique)');

		_.each( s , function( dom ){
			sliders.sliders.push(sliders.buildSlider( dom ));
		});

	},

	customSlider: function( dom , conf ){
		return sliders.sliders.push( sliders.buildSlider( dom , conf ) );
	},

	buildSlider: function( dom , conf ){

		dom = dom || $('.owl-carousel').eq(0);

		if( conf ){
			conf = _.extend( sliders.defaultConfig , conf );
		}
		else {
			conf = sliders.defaultConfig;
		}

		// Uncomment to get the build config of each slider
		return $( dom ).owlCarousel( conf );
	},

	thumbnailPagination: function( slider ){

		var items = slider.find('.owl-item').not('.cloned');
		var paging = slider.find('.owl-dots .owl-dot');

		_.each( items , function( slide , key ){

			if(typeof($(slide).find('img').eq(0).attr('data-thumb'))!='undefined') {
				var thumb = $(slide).find('img').eq(0).attr('data-thumb');
			} else {
				var thumb = $(slide).find('img').eq(0).attr('src');
			}

			var dot = $( paging[ key ] );
			dot.addClass('thumbnail');

			if($(slide).find('.mobile-video-container').length > 0) {
				dot.append('<a href="javascript:return false;" class="overlay-button center play-button thumb"></a>');
			}

			dot.find('span').css({
				backgroundImage: 'url('+ thumb +')',
				backgroundPosition: 'center'
			});

			dot.find('span').hover(function(){
				$(this).css('background-size','cover');

			}, function(){
				$(this).css('background-size','cover')
			}
			);
		});

	},

	resizeThumbnailPaginationMobile: function() {

        if( window.matchMedia){
            var mq = window.matchMedia( "(max-width: 768px)" );
            if( mq.matches ){
				$('.owl-dots').each(function() {
					var dots = $(this).find('.owl-dot.thumbnail').length;
					$(this).find('.owl-dot.thumbnail').each(function() {
						$(this).css('width', (100 / dots) + '%');
					});
				});
			}
			else {
				$('.owl-dots').each(function() {
					$(this).find('.owl-dot.thumbnail').each(function() {
						$(this).css('width', 'initial');
					});
				});
			}
		}
	},

	textPagination: function( slider ){

		var items = slider.find('.owl-item').not('.cloned');
		var paging = slider.find('.owl-dots .owl-dot');

		_.each( items , function( slide , key ){
			var text = $(slide).find('.item').attr('title') || $(slide).find('img').eq(0).attr('alt');
			var dot = $( paging[ key ] );
			dot.addClass('text');
			dot.css('width', (100 / items.length) + '%');

			dot.find('span').html(text);
		});

	},

	heightception: function (){

		$('.owl-dots').each(function() {
			$(this).height('auto');
			$(this).height($(this).height());
		});
	},

	matchHeight: function( slider ) {

		maxHeight = 0;

		slider.find('.owl-item.active').each(function() {

			var elementHeight = $(this).outerHeight(true);

			if(elementHeight > maxHeight) {
				maxHeight = elementHeight;
			}

		});

		slider.find('.owl-item .item').each(function() {
			$(this).css('min-height',maxHeight);
		});
	},

	onSliderLoad: function() {},
	onInitialize : function(){},
	onInitialized : function(){

		if( this.settings.thumbnailPagination ){
			sliders.thumbnailPagination( this.$element );
		}

		if( this.settings.textPagination ){
			sliders.textPagination( this.$element );
			sliders.heightception();
		}
        
		if( this.settings.onSliderLoad ){
			this.settings.onSliderLoad();
		}
	},
	onResize : function(){},
	onResized : function(){},
	onRefresh : function(){},
	onRefreshed : function(){},
	onDrag : function(){},
	onDragged : function(){},
	onTranslate : function(){},
	onTranslated : function(){},
	onChange : function(){},
	onChanged : function(){},
	onPlayVideo : function(){},
	onStopVideo : function(){},
};

sliders.defaultConfig = {
	loop         : false,
	margin       : 5,
	items        : 1,
	dots 		 : false,
	onSliderLoad : sliders.onSliderLoad,
	onInitialize : sliders.onInitialize,
	onInitialized: sliders.onInitialized,
	onResize     : sliders.onResize,
	onResized    : sliders.onResized,
	onRefresh    : sliders.onRefresh,
	onRefreshed  : sliders.onRefreshed,
	onDrag       : sliders.onDrag,
	onDragged    : sliders.onDragged,
	onTranslate  : sliders.onTranslate,
	onTranslated : sliders.onTranslated,
	onChange     : sliders.onChange,
	onChanged    : sliders.onChanged,
	onPlayVideo  : sliders.onPlayVideo,
	onStopVideo  : sliders.onStopVideo,
	thumbnailPagination: false,
	textPagination: false,
	navText: false
};

sliders.init();