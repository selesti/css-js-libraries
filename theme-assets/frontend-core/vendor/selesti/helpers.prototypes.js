/* DOM Prototypes */

String.prototype.contains = String.prototype.contains || function() {
    return String.prototype.indexOf.apply(this, arguments) !== -1;
};

String.prototype.ltrim = function() {
    return this.replace(/^\s+/, '');
};

String.prototype.rtrim = function() {
    return this.replace(/\s+$/, '');
};

String.prototype.trim = function() {
    return this.ltrim().rtrim();
};

String.prototype.encode = function() {
    return (this.length > 0) ? encodeURIComponent(this) : this;
};

String.prototype.stripTags = function() {
    return this.replace(/<\S[^>]*>/g, "");
};

String.prototype.replaceAll = function(search, replace) {
    if (!replace) return this;
    return this.replace(new RegExp('[' + search + ']', 'g'), replace);
};

String.prototype.padLeft = function(length, character) {
    return new Array(length - this.length + 1).join(character || ' ') + this;
};

String.prototype.padRight = function(length, character) {
    return this + new Array(length - this.length + 1).join(character || ' ');
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Date.prototype.toClean = function() {
    if (this !== null) {
        var vDay = ((this.getDate()) < 10) ? '0' + (this.getDate()) : (this.getDate()),
            oMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            vMonth = oMonths[this.getMonth()],
            vYear = this.getFullYear().toString().right(2);

        return vDay + ' ' + vMonth + ' \'' + vYear;
    } else {
        return '[Invalid Date]';
    }
};

Date.prototype.toSQL = function() {
    var vDay = ((this.getDate()) < 10) ? '0' + (this.getDate()) : (this.getDate()),
        nMonth = (this.getMonth() + 1),
        vMonth = (nMonth < 10) ? '0' + nMonth : nMonth,
        vYear = this.getFullYear().toString(),
        vHours = ((this.getHours()) < 10) ? '0' + (this.getHours()) : (this.getHours()),
        vMinutes = ((this.getMinutes()) < 10) ? '0' + (this.getMinutes()) : (this.getMinutes()),
        vSeconds = ((this.getSeconds()) < 10) ? '0' + (this.getSeconds()) : (this.getSeconds());

    return vDay + '/' + vMonth + '/' + vYear + ' ' + vHours + ':' + vMinutes + ':' + vSeconds;
};

Array.prototype.first = function() {
    return this[0] || undefined;
};

Array.prototype.last = function() {
    if (this.length > 0) {
        return this[this.length - 1];
    }
    return undefined;
};

Array.prototype.chunk = function(chunkSize) {
    var array=this;
    return [].concat.apply([],
        array.map(function(elem,i) {
            return i%chunkSize ? [] : [array.slice(i,i+chunkSize)];
        })
    );
};

/* Custom BlockUI Functions */
(function( $ ){

   $.fn.blockui = $.fn.blockUI = function() {
      this.addClass('block-ui-target block-ui-block');
      return this;
   };

   $.fn.unblockui = $.fn.unblockUI = function() {
      this.removeClass('block-ui-target block-ui-block');
      return this;
   }; 
})( jQuery );