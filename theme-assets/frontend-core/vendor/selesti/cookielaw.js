(function($){
    $.cookielaw = $.fn.cookielaw = function(options){
        cookie = {
            settings : $.extend({
                    theme   :   "light",
                    msg     :   "By continuing to use this website you\'re agreeing to allow us to store cookies on your system.",
                    link    :   "/cookies",
                    button  :   "Agree &amp; Continue",
                    width   :   940,
                    reset   :   $('.cookie-notification-reset')
                }, options),
            init : function(){
                var data = cookie.localStorage.getItem('cookiedata');
                cookie.settings.reset.on('click', cookie.reset);
                if ( data == null || !data ) { cookie.notification(); }
                else { cookie.agreed(); }
            },
            notification: function(){
                var html =        '<div id="cookie-notification" class="cookie-notification-wrapper ' + cookie.settings.theme + '" style="max-width: ' + cookie.settings.width + 'px">';
                    html = html + '<p class="cookie-notification-message">' + cookie.settings.msg + ' <a class="cookie-notification-link" href="' + cookie.settings.link + '" title="Read our full cookie policy" >More on cookies</a></p>';
                    html = html + '<button class="cookie-notification-button">' + cookie.settings.button + '</button>';
                    html = html + '</div>';
                cookie.build(html);
            },
            agreed: function(){
                //Any things that need doing if the are already accepted.
            },
            build: function(string){
                $('body').prepend(string);
                $('.cookie-notification-wrapper').slideDown();
                $('.cookie-notification-button').on('click', cookie.accept);
            },
            accept: function(){
                cookie.localStorage.setItem('cookiedata', true);
                $('.cookie-notification-wrapper').slideUp(function(){
                    $(this).remove();
                });
            },
            reset: function(){
                cookie.localStorage.removeItem('cookiedata');
                window.location = window.location;
            },
            localStorage: {
                setItem: function(key, value){
                    try {
                        return localStorage.setItem(key, value);
                    }
                    catch(e){}
                },
                removeItem: function(key){
                    try {
                        return localStorage.removeItem(key);
                    }
                    catch(e){}
                },
                getItem: function(key){
                    try {
                        return localStorage.getItem(key);
                    }
                    catch(e){}
                }
            }
        };
        cookie.init();
    };
}( jQuery ));