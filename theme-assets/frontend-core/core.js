var Helpers = Helpers || function(){};

var SelestiCore = function(){

    this.helpers = new Helpers();
    this.bind = this.bind;
    Pace && Pace.once('done', this.paceComplete);

    var self = this;

    jQuery(function(){
        self.helpers.maxLengthPlugin();
        self.helpers.inputIsURL();
    });
};

SelestiCore.prototype.paceComplete = function(){
    var body = document.getElementsByTagName('body')[0];
    body.className = body.className + ' loaded';
};

SelestiCore.prototype.ready = function( callBack ){
    callBack = callBack || null;
    if(callBack) jQuery( document ).ready(callBack);
};

SelestiCore.prototype.loaded = function( callBack ){
    callBack = callBack || null;
    if(callBack) jQuery( window ).load(callBack);
};

SelestiCore.prototype.resize = function( callBack ){
    callBack = callBack || null;
    if(callBack) jQuery( window ).resize(callBack);
};

var selesti = new SelestiCore();

//@prepros-prepend helpers.js
//@prepros-prepend vendor/html5shiv/3.7.2/html5shiv.js
//@prepros-prepend vendor/modernizr/2.8.3/modernizr.min.js
//@prepros-prepend vendor/underscorejs/1.7/underscore.min.js
//@prepros-prepend vendor/humanize/1.0/humanize.min.js
//@prepros-prepend vendor/velocity/1.1.0/velocity.min.js
//@prepros-prepend vendor/velocity/1.1.0/velocity.ui.min.js
//@prepros-prepend vendor/angularjs/1.3.8/angular.min.js
//@prepros-prepend vendor/owlcarousel/2.0.0.2.4/owl.carousel.min.js
//@prepros-prepend vendor/respondjs/1.4.2/respond.min.js
//@prepros-prepend vendor/selesti/cookielaw.js
//@prepros-prepend vendor/hoverintent/1.8.0/jquery.hoverIntent.js
//@prepros-prepend vendor/select2/3.5.2/select2.js
